// 导入设置过拦截器的axios
import service from "./request";

// 一个请求封装成一个函数
// 登录请求函数
export function user_login(params = {}) {
    return service.post('/user/login', params);
}

// 注册请求函数
export function user_register(params = {}) {
    return service.post('/user/register', params);
}
// 商品列表请求函数
export function shop_list(params = {}) {
    return service.get('/shop/list', { params });
}

// 爆爆团
export function tuan_list(params = {}) {
    return service.get('/tuan/list', { params })
}