// 对axios进行二次封装
import axios from "axios";

// 实例化axios
const service = axios.create({
    timeout: 10 * 1000,
    baseURL: '/api'
})

// 设置拦截器(请求拦截器)
service.interceptors.request.use((config) => {
    // 统一带请求头
    config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');

    return config;
}, (error) => {
    return Promise.reject(error);
})

// 设置拦截器(响应拦截器)
service.interceptors.response.use((res) => {
    return res;
}, (error) => {
    if (error.response.status == 401) {
        alert('身份认证过期，登录过期')
    } else if (error.response.status == 404) {
        alert('访问路径错误')
    } else if (error.response.status == 500) {
        alert('服务器发送错误')
    }
    return Promise.reject(error);
})

export default service;