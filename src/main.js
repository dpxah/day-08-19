import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入外部组件库组件
// tababr
import { Tabbar, TabbarItem } from 'vant';
import { Swipe, SwipeItem } from 'vant';

import { Form, Field, Button } from 'vant';


// 注册组件
Vue.use(Form);
Vue.use(Field);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Button);

Vue.use(Tabbar);
Vue.use(TabbarItem);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
