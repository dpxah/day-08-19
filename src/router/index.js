import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [ //定义多个一级路由
  {
    path: '/index',
    name: 'index',
    meta: { title: 'index' }, //路由元信息(会永远保存在当前路由对象身上)
    component: () => import('../views/Index.vue'),
    children: [ //嵌套路由(二级路由)
      {
        path: '/index/home',
        name: 'home',
        meta: { title: '首页' },
        component: () => import('../views/Index/Home.vue')
      },
      {
        path: '/index/order',
        name: 'order',
        meta: { title: '订单' },
        component: () => import('../views/Index/Order.vue')
      },
      {
        path: '/index/mine',
        name: 'mine',
        meta: { title: '我的' },
        component: () => import('../views/Index/Mine.vue')
      },
      {
        path: '/index/tuan',
        name: '/tuan',
        meta: { title: '爆爆团' },
        component: () => import('../views/Index/Tuan.vue')
      },
      // 二级路由重定向
      {
        path: '/index',
        redirect: '/index/home'
      },
      // 404路由
      {
        path: '*',
        component: () => import('../views/NotFound.vue')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    meta: { title: '登录页' },
    component: () => import('../views/Login.vue'),
    // 路由独享的守卫函数
    // beforeEnter: (to, from, next) => {
    //   next();
    // }
  },
  // 路由地址中带有 ：xxx 的路由地址  都属于动态路由
  // path: '/detail/:name',    // :name 是个占位符，实际在跳路由的时候，会动态拼接参数  例如：'/detail/'+100
  {
    path: '/detail',
    name: 'detail',
    meta: { title: '详情页' },
    component: () => import('../views/Detail.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: { title: '注册页' },
    component: () => import('../views/Register.vue')
  },
  // 一级路由重定向
  {
    path: '/',
    redirect: '/index'
  },
  // 404路由
  {
    path: '*',
    component: () => import('../views/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history', //设置路由模式 history  hash
  base: process.env.BASE_URL,
  routes
})

// 设置实例方法（导航守卫方法）

// 路由的全局前置守卫方法
router.beforeEach((to, from, next) => {
  // 参数一：to 新的路由对象
  // 参数二：from 旧的路由对象
  // 参数三： next 路由控制方法。 调用该方法则允许路由跳转，为调用该方法则不允许路由跳转

  // 动态更新网页标题
  document.title = to.meta.title;

  // 获取LocalStorage中的登录凭证
  var token = localStorage.getItem('token');

  // 只要在登录以后 才能跳转到 订单页
  if (to.path == '/index/order' || to.path == '/index/mine') {  //想要跳往 订单页
    if (token) { //已经登录
      next();
    } else {  //未登录
      next('/login');
    }
  } else {  //其他页面(非订单页)
    next(); //必须手动调用该函数，否则无法完成路由跳转
  }

  // 对于后台管理系统这种应用，只有登录页是可以随意访问的，但是其他所有页面都是必须在登录后才可以访问
  // if (to.path == '/login') { //跳往 登录页
  //   if (token) { //已经登录，重定向到 "/""
  //     next('/');
  //   } else { //未登录 ，允许跳往 登录页
  //     next();
  //   }
  // } else {  //跳往 非登录页
  //   if (token) { //已登录 ，允许访问
  //     next();
  //   } else { //未登录 ，重定向到 "/login"
  //     next('/login');
  //   }
  // }
  // next();
})





// 路由全局后置守卫方法
// router.afterEach((to, from) => {
//   console.log('afterEach');
// })

export default router
